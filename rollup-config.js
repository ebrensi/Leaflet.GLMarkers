
// In a console, run:
// rollup -c rollup-config.js


export default {
	entry: 'src/GLMarkers.js',
	dest: 'dist/Leaflet.GLMarkers.js',
	format: 'cjs',
		sourceMap: true,
		plugins: [
// 		require('rollup-plugin-string')({ extensions: ['.glsl'] }),
		require('rollup-plugin-buble')(),
// 		require('rollup-plugin-commonjs')(),
// 		require('rollup-plugin-node-resolve')()
		]
};

